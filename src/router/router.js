import Vue from "vue";
import VueRouter from "vue-router";
import HomePage from "../components/pages/HomePage";
import Transactions from "../components/pages/Transactions";

Vue.use(VueRouter);

const routes = [
  {
    name: "Home",
    path: "/",
    component: HomePage
  },
  {
    name: "Transactions",
    path: "/Transactions",
    component: Transactions
  }
];

const router = new VueRouter({
  mode: "history",
  base: "localhost:8080",
  routes
});

export default router;
